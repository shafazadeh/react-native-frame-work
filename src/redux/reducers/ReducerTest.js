import {store} from '../../components/App'

//import {insertItem,removeItem,insertItems} from '../helper'

export const TEST = 'my-app/test/TEST';
export const TEST_SUCCESS = 'my-app/test/TEST_SUCCESS';
export const TEST_FAIL = 'my-app/test/TEST_FAIL';

const initialState = {  test : "" };
const FormData = require('form-data');


export default function ReducerTest(state = initialState, action) {
  switch (action.type) {
  
    case TEST:
      return { ...state ,progressVisible:true, isRefreshing:true};
    case TEST_SUCCESS:    
      console.log('acts res:',action);
      return { ...state, test : "hi there"};
    case TEST_FAIL:
      return { ...state};
    
    default:
      return state;
  }
}


// export function getActivities(instaAccId) {
//   let form = new FormData();
//   form.append('data', `{"instaAccId":"${instaAccId}","type":1,"page":1,"perPage":10}`);

//   return {
//     type: GET_LIST,
//     payload: {
//       request: {
//         method: 'post',
//         url: `AppInstaAcc/GetActivity`,
//         data: form,
//         headers: {
//           'content-type': 'multipart/form-data',
//           'uid':store.getState().ReducerUser.user.result.id
//         },
//       }
//     }
//   };
// }
