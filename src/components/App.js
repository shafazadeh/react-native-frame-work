/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { createStore, applyMiddleware,combineReducers } from 'redux';
import { Provider, connect } from 'react-redux';
import { multiClientMiddleware } from 'redux-axios-middleware';
import axios from 'axios';
import Router from './Router';
import ReducerTest from '../redux/reducers/ReducerTest';
import * as constants from '../classes/Constant'

const appReducer = combineReducers({
  ReducerTest,
 
});


const middlewares = [multiClientMiddleware(
                                        {
                                          default: {
                                            client: axios.create({
                                               baseURL:constants.WsURL,
                                               responseType: 'json'
                                            })
                                          },
                                          instagram: {
                                            client: axios.create({
                                                baseURL:'https://www.instagram.com',
                                                responseType: 'json'
                                            })
                                          }
                                        }
                                      )];
export const store = createStore(
  appReducer,
  applyMiddleware(...middlewares),
);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
         <Router    />
      
      </Provider>
    );
  }
}
